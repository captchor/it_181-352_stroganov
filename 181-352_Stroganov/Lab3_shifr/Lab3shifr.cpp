/* "Шифровка" массива.
Выполнил:
Строганов Владимир
*/

//Подключение библеотек. 
#include "stdafx.h" 
#include <iostream> 
#include <Windows.h> 

//Использование пространственного имени std. 
using namespace std;

int main()
{
	//Объявление переменных 
	int i, k, shag, size;
	cout << "Enter array size (number > 0): ";
	cin >> size;
	cout << "\nEnter encryption step (number > 0): ";
	cin >> shag;

	// Выделение памяти для массива 
	int *array = new int[size];
	int *array1 = new int[size];

	// Заполняем массив с клавиатуры. 
	cout << "Enter array elements \n";
	for (int i = 0; i < size; i++) {
		cout << "[" << i + 1 << "]" << ": ";
		cin >> array[i];
	}
	cout << "\nYour array: ";

	// Вывод массива пользователя. 
	for (i = 0; i < size; i++) {
		cout << array[i] << ' ';
	}

	//"Шифрование" Массива 
	for (i = 0; i < size; i++) {

		k = i;

		if (k + shag >= size) {
			array1[k + shag - size] = array[i];
		}
		else {
			array1[k + shag] = array[i];
		}
	}

	//Вывод зашифрованного массива. 
	cout << "\nArray after encryption with step = " << shag << " : ";
	for (i = 0; i < size; i++) {
		cout << array1[i] << ' ';
	}

	// Освобождение памяти. 
	delete[] array;
	delete[] array1;

	getchar();

	return (0);
}