#include "stdafx.h" 
#include<iostream> 
using namespace std;

class Matrix
{
	int a[3][3];
public:
	void accept();
	void display();
	void operator +(Matrix x);
};
void Matrix::accept()
{
	cout << "\n Напишите матрицу (3 X 3) : \n";
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			cout << " ";
			cin >> a[i][j];
		}
	}
}
void Matrix::display()
{
	for (int i = 0; i < 3; i++)
	{
		cout << " ";
		for (int j = 0; j < 3; j++)
		{
			cout << a[i][j] << " ";
		}
		cout << "\n";
	}
}
void Matrix::operator +(Matrix x)
{
	int mat[3][3];
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			mat[i][j] = a[i][j] + x.a[i][j];
		}
	}
	cout << "\n Добавление матрицы: \n";
	for (int i = 0; i < 3; i++)
	{
		cout << " ";
		for (int j = 0; j < 3; j++)
		{
			cout << " " << mat[i][j] << " ";
		}
		cout << endl;
	}
}
int main()
{
	Matrix m, n;
	m.accept(); // приём строк 
	n.accept(); // приём колоок 
	cout << "\n Первая матрица: \n";
	m.display(); // вывод первой матрицы 
	cout << "\n Вторая матрица: \n";
	n.display(); // вывод второй матрицы 
	m + n; // сложеение 
	return 0;
}