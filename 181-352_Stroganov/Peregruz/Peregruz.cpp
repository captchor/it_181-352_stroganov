// Реализовать: перегрузить оператор сложения.

// ПЕРЕГРУЗКА ФУНКЦИЙ, МЕТОДОВ И ОПЕРАТОРОВ
// Перегрузка (overloading) - объявление нескольких функций (а также методов классов и операций
// являются функциями) с одинаковыми именами, но разным набором аргументов.

// 1. Синтаксис. Просто объявление рядом нескольких функций с идентичными именами.
// Входные параметры обязательно должны различаться.
// Различие по выходному параметру не является перегрузкой.
// Когда компилятор встречает в тексте программы вызов одной из перегруженных функций
// он подставляет одну из них, ориентируясь на список переданных параметров, как на сигнатур
// В отличие от Pascal дополнительные служебные слова не требуются.

// 2. Примеры использования
// Математические функции для float и для double
// Строковые методы для std::string и для char*
// Перегрузка конструкторов (пустой и копирующий конструктор + другие типы)
// std::string str1;
// str1.at(1);

// 3. Перегрузка операторов
// Операторы C++ являются функциями, и их можно перегружать
// Унарные и бинарные операторы перегружаются по-разному ("Солтер C++ для профисионалов")
// Унарные операторы перегружаются как метод пользовательского класса
// а бинарные - как отдельные функции

mat result;// создать новый объект для записи
		   // собственно, сложение элементов lhs и rhs в result

		   // 6. ОПРЕДЕЛЕНИЕ МЕТОДОВ СНАРУЖИ КЛАССА
		   // * в объявлении класса останется объявление (заголовок) метода
		   // * в тело (реализация) метода переносится наружу вместе с копией заголовка
		   // * к заголовку реализации добавляется "имя_класса::"

		   // 7. Ключевое слово this
		   // this может использоватся только внутри методов класса
		   // это указатель на объект, вызвавший метод

		   // 8. Указатели на объекты
		   // При работе с указателям на объект вместо точки
		   // для обращения к полям используется ->
		   // Объекты можно размещать в динамической памяти аналогично другим данным
		   // В этом случае сначала 1) объявляется указатель на объект
		   // затем 2) с помощью new объект создается и заполняется в ДП, при этом
		   // автоматически вызывается конструктор класса
		   // только после этого к полям объекта
#include <iostream>
#include <stdio.h>
#include <string>
#include <cstring>
#include <fstream>
using namespace std;
int main()
{

	mat & operator ++();
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < rows; i++)
			{
				this->data[i][j]++;
			}
		}
		return (*this);
		// Запись mat++ аналогична mat = (operator ++)(mat)
	}

	getchar();
	return 0;
}

