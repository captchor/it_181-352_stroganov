/* Сортировка массива пузырьком.
Выполнил:
//Строганов Владимир
*/
// Подключение библеотек.
 
#include "pch.h" 
#include <iostream> 

// Использование пространственного имени std. 
using namespace std;

int main()
{
	// Объявление переменных. 
	int lenght, a, i, k;
	cout « "Enter array length (number > 0): ";
	cin » lenght;

	// Выделение памяти для массива 
	int *array = new int[lenght];

	// Заполняем массив с клавиатуры. 
	cout « "Enter array elements \n";
	for (int i = 0; i < lenght; i++) {
		cout « "[" « i + 1 « "]" « ": ";
		cin » array[i];
	}
	cout « "\n Your array: ";

	// Вывод массива пользователя. 
	for (k = 0; k < lenght; k++) {
		cout « array[k] « ' ';
	}
	cout « "\n Array after sorting: ";

	// Сортировка массива пузырьком. 
	for (k = 0; k < lenght - 1; k++) {

		for (i = 0; i < lenght - 1 - k; i++) {

			if (array[i] > array[i + 1]) {
				//Меняем местами элементы массива. 
				a = array[i];
				array[i] = array[i + 1];
				array[i + 1] = a;
			}
		}
	}

	// Вывод отсортированного массива. 
	for (k = 0; k < lenght; k++) {
		cout « array[k] « ' ';
	}

	delete[] array;

	return 0;
}