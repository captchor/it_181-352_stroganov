/*

Строки и файлы

Строки C-типа массивы типа char оканчивающиеся нулевым символом инициализируются таким образом:
char cstr1[] = "C-style string1"; в конце 0 дописывается автоматом.
char cstr2[] = {'C','-', '0'};

по нулю определяется конец строки. В случае если пользователь забыл оставить \0 или он по темм или иным причинам
стерся, функции С будут пытаться считывать символы до тех пор, пока не встретят \0 или пока не вызовет ошибку.

2. Функции для работы с С-строками
вывод на печать printf(char*,,,) или printf_s(char*,,,) - print formatted
пример: printf("базовая %s строка %t", параметр1, параметр2)
знаком % помечают места вставки параметров
%f - число с плавающей точкой
%s - stroka
%d - целое число
%e - число в экспененциальном виде

// 3. ПРОЧИЕ СТРОКОВЫЕ ФУНКЦИИ
// объединение (конкатенация) строк strcat(*char, *char)
// копирование из одной строки в другую strcpy()
// сравнение strcmp ()
// длина строки strlen()
// вставка строки в строку производится в несколько действий
// scanf чтение с консоли (*char, параметр)

// 4. СТРОКА С++
// в стандартной библиотеке C++ массив символов инкапсулирован в КЛАСС std::string
// в который также собраны самые часто используемые методы обработки
// (конкатенация, подсчёт длины, вставка, удаление, поиск и т.д.)
// Примеры:
//std::string cppstr1 = "C++ - style string 1";
//std::string cppstr2 = "";

ctrl+probel = menu
ctrl+shirt+probel = help
*/


#include <iostream>
#include <stdio.h>
#include <string>
#include <cstring>
#include <fstream>
int main()
{
	char cstr1[] = "C-style string1";
	char cstr2[] = { 'C','-','C','-' ,'C','-' ,'C','-' ,'C','-','\0' };
	printf(cstr2);// Автоматом перенос не делается(надо \n)
	printf("\n\tParameter1 = %2.3f, \nparameter2 = %d, \nparameter3 = %s\n\n", 1.5, 100, "\" some string\"");
	strcat_s(cstr1, cstr2);
	char copy[255];
	char paste[] = "asdfghjklas";
	strcpy_s(copy, 255, paste);
	printf("Resultat is (%s,%s)", copy, paste);
	strcmp(cstr1, cstr2);
	strlen(cstr2);
	//scanf(cstr1);

	//5
	
	std::fstream fs;
	fs.open("example_textfile.mylife", //путь к файлу и имя
		std::ios::in | std::ios::out | std::ios::trunc); //режим работы с файлом (комбинация чтения и записи)
	fs << "100500" << std::endl;
	fs << "some_string" << std::endl;
	fs << "1234566.789123" << std::endl;

	fs.seekg(0); //перейти обратно в начало
	int int1; fs >> int1; std::cout << "первая запись в файле : " << int1 << std::endl;
	char buffer[255]; fs >> buffer; std::cout << "первая запись в файле: " << int1 << std::endl;
	fs.close();

	return 0;
}

